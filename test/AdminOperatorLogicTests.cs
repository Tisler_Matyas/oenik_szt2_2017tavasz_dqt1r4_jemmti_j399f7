﻿// <copyright file="AdminOperatorLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using data.Interface;
    using data.Model;
    using logic.Helpers;
    using logic.Interfaces;
    using logic.LogicClasses;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class AdminOperatorLogicTests
    {
        [Test]
        public void AdminETesztIgen()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            IAdminOperator logic = new AdminOperatorLogic(testUserRepo.Object, null, null, null);
            Assert.That(logic.AdminE(new Guid("9D2B0228-4D0D-4C23-8B49-01A698857111")), Is.EqualTo(true));
        }

        [Test]
        public void AdminETesztNem()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            IAdminOperator logic = new AdminOperatorLogic(testUserRepo.Object, null, null, null);
            Assert.That(logic.AdminE(new Guid("9D2B0228-4D0D-4C23-8B49-01A698857709")), Is.EqualTo(false));
        }

        [Test]
        public void AllProductsTeszt()
        {
            Mock<IProduct> testProductRepo = this.ProductSetup();
            IAdminOperator logic = new AdminOperatorLogic(null, testProductRepo.Object, null, null);

            Assert.That(logic.AllProducts().Count, Is.EqualTo(3));
        }

        [Test]
        public void ModProductTeszt()
        {
            Mock<IProduct> testProductRepo = this.ProductSetup();
            Mock<ICategory> testCategoryRepo = this.CategorySetup();
            IAdminOperator logic = new AdminOperatorLogic(null, testProductRepo.Object, null, testCategoryRepo.Object);

            Product product1 = new Product();
            product1.ProductID = Guid.NewGuid();
            product1.CategoryID = new Guid("7e3262ae-91ee-4a7a-93e9-fd61463a190e");
            product1.Title = "Legjobb akció/vígjáték film";
            product1.ReleaseYear = 2014;
            product1.Description = "Legjobb akcio film a világon";
            product1.IMDB = 6;
            product1.Category = "Akcio";
            product1.Price = 400;

            try
            {
                logic.ModProduct(product1);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        [Test]
        public void SaveProductTeszt()
        {
            Mock<IProduct> testProductRepo = this.ProductSetup();
            Mock<ICategory> testCategoryRepo = this.CategorySetup();
            IAdminOperator logic = new AdminOperatorLogic(null, testProductRepo.Object, null, testCategoryRepo.Object);

            Product product1 = new Product();
            product1.ProductID = new Guid("9D2A0228-4D0D-4C23-8B49-01A691954444");
            product1.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            product1.Title = "Legjobb akció/vígjáték film";
            product1.ReleaseYear = 2012;
            product1.Description = "Egész jó akció film";
            product1.IMDB = 2;
            product1.Category = "Vígjáték";
            product1.Price = 555;

            try
            {
                logic.SaveProduct(product1);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        public void PaymentApprovementRequests()
        {
            Mock<IBalance> testBalanceRepo = this.BalanceHistorySetup();
            IAdminOperator logic = new AdminOperatorLogic(null, null, testBalanceRepo.Object, null);

            var notApproved = logic.PaymentApprovementRequests();

            Assert.That(notApproved.Count, Is.EqualTo(2));
        }

        public Mock<IUser> UserSetup()
        {
            List<User> userek = new List<User>();

            User user1 = new User();
            user1.UserID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698857709");
            user1.FullName = "Toth János";
            user1.Username = "toth";
            user1.Password = PasswordHelper.ComputeHash("toth", new SHA256CryptoServiceProvider());
            user1.Email = "toth.janos@gmail.com";
            user1.IsAdmin = false;
            user1.Balance = 2000;
            user1.BalanceHistory = new List<BalanceHistory>();
            user1.History = new List<History>();

            userek.Add(user1);

            User user2 = new User();
            user2.UserID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698857111");
            user2.FullName = "Tislér Mátyás";
            user2.Username = "tisler";
            user2.Password = "tisler";
            user2.Email = "tisler.matyas@gmail.com";
            user2.IsAdmin = true;
            user2.Balance = 40;
            user2.BalanceHistory = new List<BalanceHistory>();
            user2.History = new List<History>();

            userek.Add(user2);

            User user3 = new User();
            user3.UserID = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            user3.FullName = "Szűcs Sándor";
            user3.Username = "szucs";
            user3.Password = "szucs";
            user3.Email = "szucs.sandor@gmail.com";
            user3.IsAdmin = false;
            user3.Balance = 500;
            user3.BalanceHistory = new List<BalanceHistory>();

            List<History> historyk = new List<History>();

            History history1 = new History();
            history1.HistoryID = Guid.NewGuid();
            history1.ProductID = new Guid("9D2A0228-4D0D-4C23-8B49-01A698854444");
            history1.UserID = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            history1.User = user3;
            history1.Price = 200;
            history1.OrderDate = DateTime.Now;
            historyk.Add(history1);

            History history2 = new History();
            history2.HistoryID = Guid.NewGuid();
            history2.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A778854444");
            history2.UserID = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            history2.User = user3;
            history2.Price = 500;
            history2.OrderDate = DateTime.Now;
            historyk.Add(history2);

            History history3 = new History();
            history3.HistoryID = Guid.NewGuid();
            history3.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A708854444");
            history3.UserID = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            history3.User = user3;
            history3.Price = 1000;
            history3.OrderDate = DateTime.Now;
            historyk.Add(history3);

            user3.History = historyk;

            userek.Add(user3);

            Mock<IUser> mockFelhasznalok = new Mock<IUser>();
            mockFelhasznalok.Setup(x => x.GetAll()).Returns(userek.AsQueryable());

            return mockFelhasznalok;
        }

        public Mock<IProduct> ProductSetup()
        {
            List<Product> productok = new List<Product>();

            Product product1 = new Product();
            product1.ProductID = new Guid("9D2A0228-4D0D-4C23-8B49-01A698854444");
            product1.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            product1.Title = "Legjobb akcio film";
            product1.ReleaseYear = 2014;
            product1.Description = "Legjobb akcio film a világon";
            product1.IMDB = 6;
            product1.Category = "Akcio";
            product1.Price = 400;

            productok.Add(product1);

            Product product2 = new Product();
            product2.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A778854444");
            product2.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            product2.Title = "Legrosszabb akcio film";
            product2.ReleaseYear = 2018;
            product2.Description = "Legrosszabb akcio film a világon";
            product2.IMDB = 3;
            product2.Category = "Akcio";
            product2.Price = 200;

            productok.Add(product2);

            Product product3 = new Product();
            product3.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A708854444");
            product3.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698855555");
            product3.Title = "Legjobb vígjáték film";
            product3.ReleaseYear = 2014;
            product3.Description = "Legjobb vígjáték film a világon";
            product3.IMDB = 9;
            product3.Category = "Vígjáték";
            product3.Price = 1200;

            productok.Add(product3);

            Mock<IProduct> mockProduct = new Mock<IProduct>();
            mockProduct.Setup(x => x.GetAll()).Returns(productok.AsQueryable());
            return mockProduct;
        }

        public Mock<IBalance> BalanceHistorySetup()
        {
            List<BalanceHistory> bhistory = new List<BalanceHistory>();

            BalanceHistory history1 = new BalanceHistory();
            history1.BalanceHistoryID = Guid.NewGuid();
            history1.IsApproved = false;
            history1.OrderDate = DateTime.Now;
            history1.Price = 400;
            history1.User = null;
            history1.UserID = Guid.NewGuid();

            bhistory.Add(history1);

            BalanceHistory history2 = new BalanceHistory();
            history2.BalanceHistoryID = Guid.NewGuid();
            history2.IsApproved = true;
            history2.OrderDate = DateTime.Now;
            history2.Price = 1400;
            history2.User = null;
            history2.UserID = Guid.NewGuid();

            bhistory.Add(history2);

            BalanceHistory history3 = new BalanceHistory();
            history3.BalanceHistoryID = Guid.NewGuid();
            history3.IsApproved = false;
            history3.OrderDate = DateTime.Now;
            history3.Price = 465;
            history3.User = null;
            history3.UserID = Guid.NewGuid();

            bhistory.Add(history3);

            Mock<IBalance> mockBalance = new Mock<IBalance>();
            mockBalance.Setup(x => x.GetAll()).Returns(bhistory.AsQueryable());
            return mockBalance;
        }

        private Mock<ICategory> CategorySetup()
        {
            List<Category> categories = new List<Category>();
            Category c1 = new Category();
            c1.CategoryID = Guid.NewGuid();
            c1.Name = "Akcio";
            Category c2 = new Category();
            c2.CategoryID = Guid.NewGuid();
            c2.Name = "Vígjáték";
            categories.Add(c1);
            categories.Add(c2);

            Mock<ICategory> mockCategories = new Mock<ICategory>();
            mockCategories.Setup(x => x.GetAll()).Returns(categories.AsQueryable());
            return mockCategories;
        }
    }
}
