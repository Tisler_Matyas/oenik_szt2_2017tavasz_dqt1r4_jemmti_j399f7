﻿// <copyright file="UserOperatorLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using data.Interface;
    using data.Model;
    using Exceptions;
    using feleves_feladat;
    using logic.Helpers;
    using logic.LogicClasses;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class UserOperatorLogicTests
    {
        /// <summary>
        /// n
        /// </summary>
        [Test]
        public void EgyenlegLekeresEgyezikTeszt()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, null, null);

            var egyenleg = logic.EgyenlegLekeres(new Guid("9D2B0228-4D0D-4C23-8B49-01A698857709"));

            Assert.That(egyenleg, Is.EqualTo(2000));
        }

        [Test]
        public void EgyenlegLekeresNemEgyezikTeszt()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, null, null);

            var egyenleg = logic.EgyenlegLekeres(new Guid("9D2B0228-4D0D-4C23-8B49-01A698857709"));

            Assert.That(egyenleg, Is.Not.EqualTo(3000));
        }

        [Test]
        public void EgyenlegLekeresNincsFelhasznaloTeszt()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, null, null);
            try
            {
                var egyenleg = logic.EgyenlegLekeres(Guid.NewGuid());
                Assert.Fail();
            }
            catch (Exception)
            {
            }
        }

        [Test]
        public void UserNevLekeresEgyezikTeszt()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, null, null);

            var nev = logic.UserNevLekeres(new Guid("9D2B0228-4D0D-4C23-8B49-01A698857709"));

            Assert.That(nev, Is.EqualTo("Toth János"));
        }

        [Test]
        public void UserNevLekeresNemEgyezikTeszt()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, null, null);

            var nev = logic.UserNevLekeres(new Guid("9D2B0228-4D0D-4C23-8B49-01A698857709"));

            Assert.That(nev, Is.Not.EqualTo("Tislér Mátyás"));
        }

        [Test]
        public void UserNevLekeresNemLetezoUserTeszt()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, null, null);
            try
            {
                var nev = logic.UserNevLekeres(Guid.NewGuid());
                Assert.Fail();
            }
            catch (Exception)
            {
            }
        }

        [Test]
        public void UserLekeresEgyezikTeszt()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, null, null);

            var id = logic.UserLekeres("toth", "toth");

            Assert.That(id, Is.EqualTo(new Guid("9D2B0228-4D0D-4C23-8B49-01A698857709")));
        }

        [Test]

        public void UserLekeresNemEgyezikTeszt()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, null, null);

            var id = logic.UserLekeres("toth", "toth");

            Assert.That(id, Is.Not.EqualTo(Guid.NewGuid()));
        }

        [Test]
        public void UserLekeresNemLetezoUserTeszt()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, null, null);
            try
            {
                var id = logic.UserLekeres("Toth Géza", "toth");
                Assert.Fail();
            }
            catch (Exception)
            {
            }
        }

        [Test]
        public void TartalomVasarlasSikeresTeszt()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            Mock<IProduct> testProductRepo = this.ProductSetup();
            Mock<IHistory> testHistoryRepo = this.HistorySetup();
            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, testProductRepo.Object, testHistoryRepo.Object);
            try
            {
                logic.TartalomVasarlas(new Guid("9D2B0228-4D0D-4C23-8B49-01A698857709"), new Guid("9D2A0228-4D0D-4C23-8B49-01A698854444"));
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        [Test]
        public void TartalomVasarlasNincsElegEgyenlegTeszt()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            Mock<IProduct> testProductRepo = this.ProductSetup();
            Mock<IHistory> testHistoryRepo = this.HistorySetup();
            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, testProductRepo.Object, testHistoryRepo.Object);
            try
            {
                logic.TartalomVasarlas(new Guid("9D2B0228-4D0D-4C23-8B49-01A698857111"), new Guid("9D2B0228-4D0D-4C23-8B49-01A708854444"));
                Assert.Fail();
            }
            catch (UserHasNotEnoughMoneyException)
            {
            }
        }

        [Test]
        public void TartalomLekeresSikeres()
        {
            Mock<IUser> testUserRepo = this.UserSetup();
            Mock<IHistory> testHistoryRepo = this.HistorySetup();
            Mock<IProduct> testProductRepo = this.ProductSetup();

            IUserOperator logic = new UserOperatorLogic(null, testUserRepo.Object, null, testProductRepo.Object, testHistoryRepo.Object);

            Guid guid = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            var lekert_tartalom = logic.TartalomLekeres(guid);

            List<Product> productok = new List<Product>();

            Product product1 = new Product();
            product1.ProductID = new Guid("9D2A0228-4D0D-4C23-8B49-01A698854444");
            product1.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            product1.Title = "Legjobb akcio film";
            product1.ReleaseYear = 2014;
            product1.Description = "Legjobb akcio film a világon";
            product1.IMDB = 6;
            product1.Category = "Akcio";
            product1.Price = 400;

            productok.Add(product1);

            Product product2 = new Product();
            product2.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A778854444");
            product2.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            product2.Title = "Legrosszabb akcio film";
            product2.ReleaseYear = 2018;
            product2.Description = "Legrosszabb akcio film a világon";
            product2.IMDB = 3;
            product2.Category = "Akcio";
            product2.Price = 200;

            productok.Add(product2);

            Product product3 = new Product();
            product3.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A708854444");
            product3.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698855555");
            product3.Title = "Legjobb vígjáték film";
            product3.ReleaseYear = 2014;
            product3.Description = "Legjobb vígjáték film a világon";
            product3.IMDB = 9;
            product3.Category = "Vígjáték";
            product3.Price = 1200;

            productok.Add(product3);

            Assert.That(lekert_tartalom.Count, Is.EqualTo(productok.Count));
        }

        [Test]
        public void KategoriaLekeresTeszt()
        {
            Mock<ICategory> testCategoryRepo = this.CategorySetup();
            IUserOperator logic = new UserOperatorLogic(null, null, testCategoryRepo.Object, null, null);

            var categoryk = logic.KategoriaLekeres();

            List<Category> kategoriak = new List<Category>();

            Category category1 = new Category();
            category1.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            category1.Name = "Akcio";
            category1.Product = null;

            kategoriak.Add(category1);

            Category category2 = new Category();
            category2.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698855555");
            category2.Name = "Vígjáték";
            category2.Product = null;

            kategoriak.Add(category2);

            Category category3 = new Category();
            category3.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698856666");
            category3.Name = "Dráma";
            category3.Product = null;

            kategoriak.Add(category3);

            Assert.That(categoryk.Count == kategoriak.Count, Is.EqualTo(true));
        }

        [Test]
        public void FilteredProductsSikeresTeszt()
        {
            Mock<IProduct> testProductRepo = this.ProductSetup();
            Mock<ICategory> testCategoryRepo = this.CategorySetup();
            IUserOperator logic = new UserOperatorLogic(null, null, testCategoryRepo.Object, testProductRepo.Object, null);

            var lekert_productok = logic.FilteredProducts("Akcio");

            List<Product> productok = new List<Product>();

            Product product1 = new Product();
            product1.ProductID = new Guid("9D2A0228-4D0D-4C23-8B49-01A698854444");
            product1.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            product1.Title = "Legjobb akcio film";
            product1.ReleaseYear = 2014;
            product1.Description = "Legjobb akcio film a világon";
            product1.IMDB = 6;
            product1.Category = "Akcio";
            product1.Price = 400;

            productok.Add(product1);

            Product product2 = new Product();
            product2.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A778854444");
            product2.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            product2.Title = "Legrosszabb akcio film";
            product2.ReleaseYear = 2018;
            product2.Description = "Legrosszabb akcio film a világon";
            product2.IMDB = 3;
            product2.Category = "Akcio";
            product2.Price = 200;

            productok.Add(product2);

            Assert.That(lekert_productok.Count == productok.Count, Is.EqualTo(true));
        }

        [Test]
        public void FilteredProductsRosszKategoriaTeszt()
        {
            Mock<IProduct> testProductRepo = this.ProductSetup();
            Mock<ICategory> testCategoryRepo = this.CategorySetup();
            IUserOperator logic = new UserOperatorLogic(null, null, testCategoryRepo.Object, testProductRepo.Object, null);

            List<Product> productok = new List<Product>();

            Product product1 = new Product();
            product1.ProductID = new Guid("9D2A0228-4D0D-4C23-8B49-01A698854444");
            product1.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            product1.Title = "Legjobb akcio film";
            product1.ReleaseYear = 2014;
            product1.Description = "Legjobb akcio film a világon";
            product1.IMDB = 6;
            product1.Category = "Akcio";
            product1.Price = 400;

            productok.Add(product1);

            Product product2 = new Product();
            product2.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A778854444");
            product2.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            product2.Title = "Legrosszabb akcio film";
            product2.ReleaseYear = 2018;
            product2.Description = "Legrosszabb akcio film a világon";
            product2.IMDB = 3;
            product2.Category = "Akcio";
            product2.Price = 200;

            productok.Add(product2);
            try
            {
                var lekert_productok = logic.FilteredProducts("VígAkcio");
                Assert.Fail();
            }
            catch (Exception)
            {
            }
        }

        [Test]
        public void NewBalanceHistoryTeszt()
        {
            Mock<IBalance> testBalanceRepo = this.BalanceHistorySetup();
            IUserOperator logic = new UserOperatorLogic(testBalanceRepo.Object, null, null, null, null);

            try
            {
                logic.NewBalanceHistory(new Guid("9D2B0228-4D0D-4C23-8B49-01A698857709"), 400, DateTime.Now, "4444-6666", "Toth János");
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        public Mock<IUser> UserSetup()
        {
            List<User> userek = new List<User>();

            User user1 = new User();
            user1.UserID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698857709");
            user1.FullName = "Toth János";
            user1.Username = "toth";
            user1.Password = PasswordHelper.ComputeHash("toth", new SHA256CryptoServiceProvider());
            user1.Email = "toth.janos@gmail.com";
            user1.IsAdmin = false;
            user1.Balance = 2000;
            user1.BalanceHistory = new List<BalanceHistory>();
            user1.History = new List<History>();

            userek.Add(user1);

            User user2 = new User();
            user2.UserID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698857111");
            user2.FullName = "Tislér Mátyás";
            user2.Username = "tisler";
            user2.Password = "tisler";
            user2.Email = "tisler.matyas@gmail.com";
            user2.IsAdmin = false;
            user2.Balance = 40;
            user2.BalanceHistory = new List<BalanceHistory>();
            user2.History = new List<History>();

            userek.Add(user2);

            User user3 = new User();
            user3.UserID = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            user3.FullName = "Szűcs Sándor";
            user3.Username = "szucs";
            user3.Password = "szucs";
            user3.Email = "szucs.sandor@gmail.com";
            user3.IsAdmin = false;
            user3.Balance = 500;
            user3.BalanceHistory = new List<BalanceHistory>();

            List<History> historyk = new List<History>();

            History history1 = new History();
            history1.HistoryID = Guid.NewGuid();
            history1.ProductID = new Guid("9D2A0228-4D0D-4C23-8B49-01A698854444");
            history1.UserID = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            history1.User = user3;
            history1.Price = 200;
            history1.OrderDate = DateTime.Now;
            historyk.Add(history1);

            History history2 = new History();
            history2.HistoryID = Guid.NewGuid();
            history2.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A778854444");
            history2.UserID = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            history2.User = user3;
            history2.Price = 500;
            history2.OrderDate = DateTime.Now;
            historyk.Add(history2);

            History history3 = new History();
            history3.HistoryID = Guid.NewGuid();
            history3.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A708854444");
            history3.UserID = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            history3.User = user3;
            history3.Price = 1000;
            history3.OrderDate = DateTime.Now;
            historyk.Add(history3);

            user3.History = historyk;

            userek.Add(user3);

            Mock<IUser> mockFelhasznalok = new Mock<IUser>();
            mockFelhasznalok.Setup(x => x.GetAll()).Returns(userek.AsQueryable());

            return mockFelhasznalok;
        }

        public Mock<IProduct> ProductSetup()
        {
            List<Product> productok = new List<Product>();

            Product product1 = new Product();
            product1.ProductID = new Guid("9D2A0228-4D0D-4C23-8B49-01A698854444");
            product1.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            product1.Title = "Legjobb akcio film";
            product1.ReleaseYear = 2014;
            product1.Description = "Legjobb akcio film a világon";
            product1.IMDB = 6;
            product1.Category = "Akcio";
            product1.Price = 400;

            productok.Add(product1);

            Product product2 = new Product();
            product2.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A778854444");
            product2.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            product2.Title = "Legrosszabb akcio film";
            product2.ReleaseYear = 2018;
            product2.Description = "Legrosszabb akcio film a világon";
            product2.IMDB = 3;
            product2.Category = "Akcio";
            product2.Price = 200;

            productok.Add(product2);

            Product product3 = new Product();
            product3.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A708854444");
            product3.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698855555");
            product3.Title = "Legjobb vígjáték film";
            product3.ReleaseYear = 2014;
            product3.Description = "Legjobb vígjáték film a világon";
            product3.IMDB = 9;
            product3.Category = "Vígjáték";
            product3.Price = 1200;

            productok.Add(product3);

            Mock<IProduct> mockProduct = new Mock<IProduct>();
            mockProduct.Setup(x => x.GetAll()).Returns(productok.AsQueryable());
            return mockProduct;
        }

        public Mock<ICategory> CategorySetup()
        {
            List<Category> kategoriak = new List<Category>();

            Category category1 = new Category();
            category1.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698854444");
            category1.Name = "Akcio";
            category1.Product = null;

            kategoriak.Add(category1);

            Category category2 = new Category();
            category2.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698855555");
            category2.Name = "Vígjáték";
            category2.Product = null;

            kategoriak.Add(category2);

            Category category3 = new Category();
            category3.CategoryID = new Guid("9D2B0228-4D0D-4C23-8B49-01A698856666");
            category3.Name = "Dráma";
            category3.Product = null;

            kategoriak.Add(category3);

            Mock<ICategory> mockCategory = new Mock<ICategory>();
            mockCategory.Setup(x => x.GetAll()).Returns(kategoriak.AsQueryable());
            return mockCategory;
        }

        public Mock<IHistory> HistorySetup()
        {
            List<History> historyk = new List<History>();

            History history1 = new History();
            history1.HistoryID = Guid.NewGuid();
            history1.ProductID = new Guid("9D2A0228-4D0D-4C23-8B49-01A698854444");
            history1.UserID = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            history1.Price = 200;
            history1.OrderDate = DateTime.Now;
            historyk.Add(history1);

            History history2 = new History();
            history2.HistoryID = Guid.NewGuid();
            history2.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A778854444");
            history2.UserID = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            history2.Price = 500;
            history2.OrderDate = DateTime.Now;
            historyk.Add(history2);

            History history3 = new History();
            history3.HistoryID = Guid.NewGuid();
            history3.ProductID = new Guid("9D2B0228-4D0D-4C23-8B49-01A708854444");
            history3.UserID = new Guid("9D2A0228-4DAA-4C23-8B49-01A698854444");
            history3.Price = 1000;
            history3.OrderDate = DateTime.Now;
            historyk.Add(history3);

            Mock<IHistory> mockHistory = new Mock<IHistory>();
            mockHistory.Setup(x => x.GetAll()).Returns(historyk.AsQueryable);
            return mockHistory;
        }

        public Mock<IBalance> BalanceHistorySetup()
        {
            List<BalanceHistory> bhistory = new List<BalanceHistory>();

            Mock<IBalance> mockBalance = new Mock<IBalance>();
            mockBalance.Setup(x => x.GetAll()).Returns(bhistory.AsQueryable());
            return mockBalance;
        }
    }
}
