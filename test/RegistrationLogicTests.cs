﻿// <copyright file="RegistrationLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using data.Interface;
    using data.Model;
    using feleves_feladat;
    using logic.LogicClasses;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class RegistrationLogicTests
    {
        public Mock<IUser> Setup()
        {
            List<User> userek = new List<User>();

            User user1 = new User();
            user1.UserID = Guid.NewGuid();
            user1.FullName = "Toth János";
            user1.Username = "toth";
            user1.Password = "toth";
            user1.Email = "toth.janos@gmail.com";
            user1.IsAdmin = false;
            user1.Balance = 2000;
            user1.BalanceHistory = new List<BalanceHistory>();
            user1.History = new List<History>();

            userek.Add(user1);

            User user2 = new User();
            user2.UserID = Guid.NewGuid();
            user2.FullName = "Tislér Mátyás";
            user2.Username = "tisler";
            user2.Password = "tisler";
            user2.Email = "tisler.matyas@gmail.com";
            user2.IsAdmin = false;
            user2.Balance = 4000;
            user2.BalanceHistory = new List<BalanceHistory>();
            user2.History = new List<History>();

            userek.Add(user2);

            User user3 = new User();
            user3.UserID = Guid.NewGuid();
            user3.FullName = "Szűcs Sándor";
            user3.Username = "szucs";
            user3.Password = "szucs";
            user3.Email = "szucs.sandor@gmail.com";
            user3.IsAdmin = false;
            user3.Balance = 500;
            user3.BalanceHistory = new List<BalanceHistory>();
            user3.History = new List<History>();

            userek.Add(user3);

            Mock<IUser> mockFelhasznalok = new Mock<IUser>();
            mockFelhasznalok.Setup(x => x.GetAll()).Returns(userek.AsQueryable());

            return mockFelhasznalok;
        }

        [Test]
        public void RegistrationTesztSikertelenEmailLetezik()
        {
            Mock<IUser> testRepo = this.Setup();
            IRegistration logic = new RegistrationLogic(testRepo.Object);

            try
            {
                logic.Registration("Tisler Matyas", "tesztuname", "tisler.matyas@gmail.com", "tisler");
                Assert.Fail();
            }
            catch (Exception)
            {
            }
        }

        [Test]
        public void RegistrationTesztSikeres()
        {
            Mock<IUser> testRepo = this.Setup();
            IRegistration logic = new RegistrationLogic(testRepo.Object);

            try
            {
                logic.Registration("Tisler Matyas", "tesztuname", "tisler.matyi@gmail.com", "tisler");
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        [Test]
        public void RegistrationTesztSikertelenUsernameLetezik()
        {
            Mock<IUser> testRepo = this.Setup();
            IRegistration logic = new RegistrationLogic(testRepo.Object);

            try
            {
                logic.Registration("Tisler Matyas", "tisler", "test@gmail.com", "tisler");
                Assert.Fail();
            }
            catch (Exception)
            {
            }
        }
    }
}
