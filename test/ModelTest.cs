﻿using data.Interface;
using logic;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using data;

namespace test
{
    [TestFixture]
    public class ModelTest
    {
        private UsersReg model;

        private Mock<IUser> m_felhasz;

        public static IEnumerable<object[]> UJUsersAdatok
        {
            get { yield return new object[] { "12122121", }; }
        }

        [SetUp]
        public void Setup()
        {
            List<Users> tesztusers = new List<Users>();

            Users user = new Users();
            user.UsersID = "0001";
            user.UsersFlag = 0;
            user.Name = "Tóth János";
            user.Email = "toth.janos@gmail.com";
            user.Password = "toth";
            user.Balance = 4000;

            tesztusers.Add(user);

            Users user2 = new Users();
            user.UsersID = "0002";
            user.UsersFlag = 0;
            user.Name = "Tislér Mátyás";
            user.Email = "tisler.matyas@gmail.com";
            user.Password = "tisler";
            user.Balance = 2000;

            tesztusers.Add(user2);

            Users user3 = new Users();
            user.UsersID = "0003";
            user.UsersFlag = 0;
            user.Name = "Szűcs Sándor";
            user.Email = "szucs.sandor@gmail.com";
            user.Password = "szucs";
            user.Balance = 500;

            tesztusers.Add(user3);

            this.m_felhasz = new Mock<IUser>();
            this.m_felhasz.Setup(x => x.GetAll()).Returns(tesztusers.AsQueryable());
            this.model = new UsersReg(this.m_felhasz.Object);
        }

        //[Test]
        //public void AddUserTeszt()
        //{
        //    string id = "0004";

        //    Assert.That(this.model.AddUser("Kis Géza", "kis", "kis.geza@gmail.com", "kis"), Is.EqualTo(id));
        //}
        [Test]
        public void EmailUtkozesVanETeszt()
        {
            string email = "tisler.matyas@gmail.com";

            Assert.That(this.model.EmailUtkozesVanE(email), Is.EqualTo(true));
        }
    }
}
