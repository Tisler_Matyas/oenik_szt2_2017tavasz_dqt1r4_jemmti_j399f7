﻿IF OBJECT_ID('Product', 'U') IS NOT NULL DROP TABLE Product;
IF OBJECT_ID('History', 'U') IS NOT NULL DROP TABLE History;
IF OBJECT_ID('BalanceHistory', 'U') IS NOT NULL DROP TABLE BalanceHistory;
IF OBJECT_ID('Product', 'U') IS NOT NULL DROP TABLE Product;
IF OBJECT_ID('Category', 'U') IS NOT NULL DROP TABLE Category;
IF OBJECT_ID('User', 'U') IS NOT NULL DROP TABLE [User];


CREATE TABLE [User](
UserID UNIQUEIDENTIFIER,
FullName VARCHAR(100),
Username VARCHAR(100),
Email VARCHAR(100),
[Password] VARCHAR(100),
Balance Numeric(20),
IsAdmin Bit,

CONSTRAINT user_id_pk PRIMARY KEY (UserID)
);

CREATE TABLE BalanceHistory(

BalanceHistoryID UNIQUEIDENTIFIER,
UserID UNIQUEIDENTIFIER,
OrderDate DateTime,
Price Numeric(30),
IsApproved Bit,

CONSTRAINT balancehistory_id_pk PRIMARY KEY (BalanceHistoryID),
CONSTRAINT balancehistory_user_fk FOREIGN KEY (UserID) REFERENCES [User] (UserID)
);

CREATE TABLE Category(
CategoryID UNIQUEIDENTIFIER,
Name VARCHAR(50),

CONSTRAINT category_id_pk PRIMARY KEY (CategoryID)
);

CREATE TABLE Product(
ProductID UNIQUEIDENTIFIER,
CategoryID UNIQUEIDENTIFIER,
Title VARCHAR(50),
ReleaseYear NUMERIC(4),
[Description] VARCHAR(MAX),
IMDB NUMERIC(2,1),
Category VARCHAR(50),
Price Numeric(10),
ContentUrl VARCHAR(MAX),
Runtime NUMERIC,
Director VARCHAR(50)

CONSTRAINT product_id_pk PRIMARY KEY (ProductID),
CONSTRAINT product_category_fk FOREIGN KEY (CategoryID) REFERENCES Category (CategoryID)
);

CREATE TABLE History(
HistoryID UNIQUEIDENTIFIER,
UserID UNIQUEIDENTIFIER,
ProductID UNIQUEIDENTIFIER,
OrderDate DateTime,
Price Numeric(30),

CONSTRAINT history_id_pk PRIMARY KEY (HistoryID),
CONSTRAINT history_user_fk FOREIGN KEY (UserID) REFERENCES [User] (UserID),
CONSTRAINT history_product_fk FOREIGN KEY (ProductID) REFERENCES Product (ProductID)
);