﻿// <copyright file="HistoryRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace data
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using data.Interface;
    using data.Model;

    public class HistoryRepository : IHistory
    {
        private MovieLabEntities context;

        public HistoryRepository()
        {
            this.context = new MovieLabEntities();
        }

        public void Save(History history)
        {
            history.HistoryID = Guid.NewGuid();
            this.context.History.Add(history);
            this.context.SaveChanges();
        }

        public void Delete(History history)
        {
            var entry = this.context.Entry(history);
            entry.State = EntityState.Deleted;
            this.context.SaveChanges();
        }

        public void Modify(History history)
        {
            this.context.History.Attach(history);
            this.context.Entry(history).State = EntityState.Modified;
            this.context.SaveChanges();
        }

        public IQueryable<History> GetAll()
        {
            this.context = new MovieLabEntities();
            return this.context.History;
        }
    }
}
