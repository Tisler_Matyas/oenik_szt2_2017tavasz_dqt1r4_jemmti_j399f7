﻿// <copyright file="CategoryRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace data.Repositories
{
    using System.Linq;
    using data.Interface;
    using data.Model;

    public class CategoryRepository : ICategory
    {
        private MovieLabEntities context;

        public CategoryRepository()
        {
            this.context = new MovieLabEntities();
        }

        public IQueryable<Category> GetAll()
        {
            this.context = new MovieLabEntities();
            return this.context.Category;
        }
    }
}
