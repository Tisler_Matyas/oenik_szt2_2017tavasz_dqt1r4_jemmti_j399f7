﻿// <copyright file="BalanceRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace data
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using data.Interface;
    using data.Model;

    public class BalanceRepository : IBalance
    {
        private MovieLabEntities context;

        public BalanceRepository()
        {
            this.context = new MovieLabEntities();
        }

        public void Save(BalanceHistory balance)
        {
            balance.BalanceHistoryID = Guid.NewGuid();
            this.context.BalanceHistory.Add(balance);
            this.context.SaveChanges();
        }

        public void Delete(BalanceHistory balance)
        {
            var entry = this.context.Entry(balance);
            entry.State = EntityState.Deleted;
            this.context.SaveChanges();
        }

        public void Modify(BalanceHistory balance)
        {
            this.context.BalanceHistory.Attach(balance);
            this.context.Entry(balance).State = EntityState.Modified;
            this.context.SaveChanges();
        }

        public IQueryable<BalanceHistory> GetAll()
        {
            this.context = new MovieLabEntities();
            return this.context.BalanceHistory;
        }

        public IQueryable<BalanceHistory> GetNotApprovedHistory()
        {
            this.context = new MovieLabEntities();
            return this.context.BalanceHistory.Where(x => !(bool)x.IsApproved);
        }
    }
}
