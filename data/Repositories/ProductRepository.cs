﻿// <copyright file="ProductRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace data
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using data.Interface;
    using data.Model;

    public class ProductRepository : IProduct
    {
        private MovieLabEntities context;

        public ProductRepository()
        {
            this.context = new MovieLabEntities();
        }

        public void Save(Product product)
        {
            product.ProductID = Guid.NewGuid();
            this.context.Product.Add(product);
            this.context.SaveChanges();
        }

        public void Delete(Product product)
        {
            var entry = this.context.Entry(product);
            entry.State = EntityState.Deleted;
            this.context.SaveChanges();
        }

        public void Modify(Product product)
        {
            // this.context.Product.Attach(product);
            var result = this.context.Product.Where(x => x.ProductID == product.ProductID).FirstOrDefault();
            if (result != null)
            {
                result.History = product.History;
                result.IMDB = product.IMDB;
                result.Price = product.Price;
                result.ReleaseYear = product.ReleaseYear;
                result.Runtime = product.Runtime;
                result.Title = product.Title;
                result.ContentUrl = product.ContentUrl;
                result.CategoryID = product.CategoryID;
                result.Description = product.Description;
                result.Director = product.Director;
                this.context.Entry(result).State = EntityState.Modified;
                this.context.SaveChanges();
            }
        }

        public IQueryable<Product> GetAll()
        {
            this.context = new MovieLabEntities();
            return this.context.Product;
        }
    }
}
