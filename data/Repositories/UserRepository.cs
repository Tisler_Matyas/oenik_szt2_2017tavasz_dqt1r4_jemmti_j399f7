﻿// <copyright file="UserRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace data
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using data.Interface;
    using data.Model;

    public class UserRepository : IUser
    {
        private MovieLabEntities context;

        public UserRepository()
        {
            this.context = new MovieLabEntities();
        }

        public void Save(User user)
        {
            user.UserID = Guid.NewGuid();
            this.context.User.Add(user);
            this.context.SaveChanges();
        }

        public void Delete(User user)
        {
            var entry = this.context.Entry(user);
            entry.State = EntityState.Deleted;
            this.context.SaveChanges();
        }

        public void Modify(User user)
        {
            var entry = this.context.User.Attach(user);
            this.context.Entry(user).State = EntityState.Modified;
            this.context.SaveChanges();
        }

        public IQueryable<User> GetAll()
        {
            this.context = new MovieLabEntities();
            return this.context.User;
        }

        public bool IsEmailExists(string email)
        {
            return this.context.User.Any(x => x.Email == email);
        }

        public bool IsUsernameExists(string username)
        {
            return this.context.User.Any(x => x.Username == username);
        }
    }
}
