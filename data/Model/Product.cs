//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace data.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Product
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            this.History = new HashSet<History>();
        }
    
        public System.Guid ProductID { get; set; }
        public Nullable<System.Guid> CategoryID { get; set; }
        public string Title { get; set; }
        public Nullable<decimal> ReleaseYear { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> IMDB { get; set; }
        public string Category { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string ContentUrl { get; set; }
        public Nullable<decimal> Runtime { get; set; }
        public string Director { get; set; }
    
        public virtual Category Category1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<History> History { get; set; }
    }
}
