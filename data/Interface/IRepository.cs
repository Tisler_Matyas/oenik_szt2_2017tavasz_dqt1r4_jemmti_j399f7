﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace data.Interface
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IRepository<T>
    {
        /// <summary>
        /// Save model. Will throw an exception if another model with the same settings
        /// exists.
        /// </summary>
        void Save(T model);

        /// <summary>
        /// Delete model.
        /// </summary>
        void Delete(T model);

        /// <summary>
        /// Modify model.
        /// </summary>
        void Modify(T model);
    }
}
