﻿// <copyright file="IBalance.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace data.Interface
{
    using System.Linq;
    using data.Model;

    public interface IBalance : IGetAll<BalanceHistory>, IRepository<BalanceHistory>
    {
        IQueryable<BalanceHistory> GetNotApprovedHistory();
    }
}
