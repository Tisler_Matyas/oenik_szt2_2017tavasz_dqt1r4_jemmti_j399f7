﻿// <copyright file="IUser.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace data.Interface
{
    using Model;

    public interface IUser : IGetAll<User>, IRepository<User>
    {
        bool IsEmailExists(string email);

        bool IsUsernameExists(string username);
    }
}
