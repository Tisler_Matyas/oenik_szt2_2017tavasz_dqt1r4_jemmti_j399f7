﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    public class CategoryNotExistsException : Exception
    {
        public CategoryNotExistsException(string message) : base(message)
        {

        }
    }
}
