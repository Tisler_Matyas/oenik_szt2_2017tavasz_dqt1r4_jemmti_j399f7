﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logic.Exceptions
{
    public class BalanceNotExistsException : Exception
    {
        public BalanceNotExistsException(string message) : base(message)
        {

        }
    }
}
