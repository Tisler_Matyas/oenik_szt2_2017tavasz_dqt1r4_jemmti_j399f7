﻿namespace feleves_feladat
{
    using System;

    public class EmailExistsException : Exception
    {
        public EmailExistsException(string message)
            : base(message)
        {
        }
    }
}
