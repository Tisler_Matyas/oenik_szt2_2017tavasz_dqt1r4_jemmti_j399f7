﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    public class UserHaveThisAlreadyException : Exception
    {
        public UserHaveThisAlreadyException(string message)
            : base(message)
        {
        }
    }
}
