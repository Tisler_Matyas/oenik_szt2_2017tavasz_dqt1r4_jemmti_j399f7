var searchData=
[
  ['iadminoperator',['IAdminOperator',['../interfacelogic_1_1_interfaces_1_1_i_admin_operator.html',1,'logic::Interfaces']]],
  ['ibalance',['IBalance',['../interfacedata_1_1_interface_1_1_i_balance.html',1,'data::Interface']]],
  ['icategory',['ICategory',['../interfacedata_1_1_interface_1_1_i_category.html',1,'data::Interface']]],
  ['igetall',['IGetAll',['../interfacedata_1_1_interface_1_1_i_get_all.html',1,'data::Interface']]],
  ['igetall_3c_20balancehistory_20_3e',['IGetAll&lt; BalanceHistory &gt;',['../interfacedata_1_1_interface_1_1_i_get_all.html',1,'data::Interface']]],
  ['igetall_3c_20category_20_3e',['IGetAll&lt; Category &gt;',['../interfacedata_1_1_interface_1_1_i_get_all.html',1,'data::Interface']]],
  ['igetall_3c_20history_20_3e',['IGetAll&lt; History &gt;',['../interfacedata_1_1_interface_1_1_i_get_all.html',1,'data::Interface']]],
  ['igetall_3c_20product_20_3e',['IGetAll&lt; Product &gt;',['../interfacedata_1_1_interface_1_1_i_get_all.html',1,'data::Interface']]],
  ['igetall_3c_20user_20_3e',['IGetAll&lt; User &gt;',['../interfacedata_1_1_interface_1_1_i_get_all.html',1,'data::Interface']]],
  ['ihistory',['IHistory',['../interfacedata_1_1_interface_1_1_i_history.html',1,'data::Interface']]],
  ['iproduct',['IProduct',['../interfacedata_1_1_interface_1_1_i_product.html',1,'data::Interface']]],
  ['iregistration',['IRegistration',['../interfacefeleves__feladat_1_1_i_registration.html',1,'feleves_feladat']]],
  ['irepository',['IRepository',['../interfacedata_1_1_interface_1_1_i_repository.html',1,'data::Interface']]],
  ['irepository_3c_20balancehistory_20_3e',['IRepository&lt; BalanceHistory &gt;',['../interfacedata_1_1_interface_1_1_i_repository.html',1,'data::Interface']]],
  ['irepository_3c_20history_20_3e',['IRepository&lt; History &gt;',['../interfacedata_1_1_interface_1_1_i_repository.html',1,'data::Interface']]],
  ['irepository_3c_20product_20_3e',['IRepository&lt; Product &gt;',['../interfacedata_1_1_interface_1_1_i_repository.html',1,'data::Interface']]],
  ['irepository_3c_20user_20_3e',['IRepository&lt; User &gt;',['../interfacedata_1_1_interface_1_1_i_repository.html',1,'data::Interface']]],
  ['itransaction',['ITransaction',['../interfacedata_1_1_interface_1_1_i_transaction.html',1,'data::Interface']]],
  ['iuser',['IUser',['../interfacedata_1_1_interface_1_1_i_user.html',1,'data::Interface']]],
  ['iuseroperator',['IUserOperator',['../interfacefeleves__feladat_1_1_i_user_operator.html',1,'feleves_feladat']]]
];
