﻿// <copyright file="MainWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat.VM
{
    using System.Security;

    public class MainWindowViewModel : BaseVM
    {
        private SecureString password;
        private string userName;

        public MainWindowViewModel()
        {
        }

        public SecureString Password
        {
            get { return this.password; }
            set { this.SetProperty(ref this.password, value); }
        }

        public string UserName
        {
            get { return this.userName; }
            set { this.SetProperty(ref this.userName, value); }
        }
    }
}
