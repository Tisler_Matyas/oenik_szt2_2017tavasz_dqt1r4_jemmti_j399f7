﻿// <copyright file="AdminViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Converters;
    using data;
    using data.Repositories;
    using logic.Interfaces;
    using logic.LogicClasses;
    using VM;

    public class AdminViewModel : BaseVM
    {
        private static AdminViewModel adminVM;

        private IAdminOperator adminLogic;
        private IUserOperator userLogic;
        private ObservableCollection<ProductVM> allProducts;
        private ProductVM selectedProduct;
        private ObservableCollection<BalanceHistoryVM> allTransactions;
        private BalanceHistoryVM selectedTransaction;
        private ObservableCollection<string> categories;
        private ProductVM tempProduct;
        private Guid userID;

        private AdminViewModel()
        {
            BalanceRepository bRepo = new BalanceRepository();
            UserRepository uRepo = new UserRepository();
            ProductRepository pRepo = new ProductRepository();
            HistoryRepository hRepo = new HistoryRepository();
            CategoryRepository cRepo = new CategoryRepository();
            this.userLogic = new UserOperatorLogic(bRepo, uRepo, cRepo, pRepo, hRepo);
            this.adminLogic = new AdminOperatorLogic(uRepo, pRepo, bRepo, cRepo);
            this.allProducts = new ObservableCollection<ProductVM>(this.adminLogic.AllProducts().Select(x => ProductToProductVMConverter.Convert(x)));
            this.categories = new ObservableCollection<string>(this.userLogic.KategoriaLekeres().Select(x => x.Name).ToList());
            this.allTransactions = new ObservableCollection<BalanceHistoryVM>(this.adminLogic.PaymentApprovementRequests().Select(x => new BalanceHistoryVM(x.BalanceHistoryID, (Guid)x.UserID, (DateTime)x.OrderDate, (int)x.Price)));
            this.categories = new ObservableCollection<string>(this.userLogic.KategoriaLekeres().Select(x => x.Name).ToList());
        }

        public static AdminViewModel AdminVM
        {
            get
            {
                if (adminVM == null)
                {
                    adminVM = new AdminViewModel();
                }

                return adminVM;
            }
        }

        public ObservableCollection<ProductVM> AllProducts
        {
            get { return this.allProducts; }
            set { this.SetProperty(ref this.allProducts, value); }
        }

        public ObservableCollection<BalanceHistoryVM> AllTransactions
        {
            get { return this.allTransactions; }
            set { this.SetProperty(ref this.allTransactions, value); }
        }

        public ProductVM SelectedProduct
        {
            get { return this.selectedProduct; }
            set { this.SetProperty(ref this.selectedProduct, value); }
        }

        public BalanceHistoryVM SelectedTransaction
        {
            get { return this.selectedTransaction; }
            set { this.SetProperty(ref this.selectedTransaction, value); }
        }

        public ObservableCollection<string> Categories
        {
            get { return this.categories; }
        }

        public ProductVM TempProduct
        {
            get { return this.tempProduct; }
            set { this.SetProperty(ref this.tempProduct, value); }
        }

        public Guid UserID
        {
            get { return this.userID; }
            set { this.SetProperty(ref this.userID, value); }
        }

        public void Refresh()
        {
            this.AllProducts = new ObservableCollection<ProductVM>(this.adminLogic.AllProducts().Select(x => ProductToProductVMConverter.Convert(x)));
            this.AllTransactions = new ObservableCollection<BalanceHistoryVM>(this.adminLogic.PaymentApprovementRequests().Select(
                x => new BalanceHistoryVM(x.BalanceHistoryID, (Guid)x.UserID, (DateTime)x.OrderDate, (int)x.Price)));
        }
    }
}
