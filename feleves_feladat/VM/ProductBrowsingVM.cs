﻿// <copyright file="ProductBrowsingVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat.VM
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class ProductBrowsingVM : BaseVM
    {
        private ObservableCollection<ProductVM> allProducts;
        private ProductVM selectedProduct;
        private ObservableCollection<ProductVM> filteredProducts;
        private bool isOwned;
        private List<string> categories;
        private string selectedCategory;

        public ProductBrowsingVM(bool isOwned)
        {
            this.isOwned = isOwned;
        }

        public bool IsOwned
        {
            get { return this.isOwned; }
            private set { this.isOwned = value; }
        }

        public List<string> Categories
        {
            get { return this.categories; }
            set { this.SetProperty(ref this.categories, value); }
        }

        public string SelectedCategory
        {
            get { return this.selectedCategory; }
            set { this.SetProperty(ref this.selectedCategory, value); }
        }

        public ObservableCollection<ProductVM> FilteredProducts
        {
            get { return this.filteredProducts; }
            set { this.SetProperty(ref this.filteredProducts, value); }
        }

        public ProductVM SelectedProduct
        {
            get { return this.selectedProduct; }
            set { this.SetProperty(ref this.selectedProduct, value); }
        }

        public ObservableCollection<ProductVM> AllProducts
        {
            get { return this.allProducts; }
            set { this.SetProperty(ref this.allProducts, value); }
        }
    }
}
