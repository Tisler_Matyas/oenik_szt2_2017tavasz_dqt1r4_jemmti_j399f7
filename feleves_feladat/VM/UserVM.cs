﻿// <copyright file="UserVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class UserVM : BaseVM
    {
        private Guid id;
        private string fullname;
        private string username;
        private string email;
        private string firstPassword;

        // private string secondPassword;
        public UserVM()
        {
        }

        public string Fullname
        {
            get { return this.fullname; }
            set { this.SetProperty(ref this.fullname, value); }
        }

        public string Username
        {
            get { return this.username; }
            set { this.SetProperty(ref this.username, value); }
        }

        public string Email
        {
            get { return this.email; }
            set { this.SetProperty(ref this.email, value); }
        }

        public string FirstPassword
        {
            get { return this.firstPassword; }
            set { this.SetProperty(ref this.firstPassword, value); }
        }

        // public string SecondPassword
        // {
        //    get { return this.secondPassword; }
        //    set { this.SetProperty(ref this.secondPassword, value); }
        // }
        public Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
    }
}
