﻿// <copyright file="BalanceHistoryVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

  public class BalanceHistoryVM : BaseVM
    {
        private int price;

        public BalanceHistoryVM(Guid iD, Guid userID, DateTime orderDate, int price)
        {
            this.ID = iD;
            this.UserID = userID;
            this.OrderDate = orderDate;
            this.Price = price;
        }

        public int Price
        {
            get { return this.price; }
            set { this.SetProperty(ref this.price, value); }
        }

        public Guid ID { get; set; }

        public Guid UserID { get; set; }

        public DateTime OrderDate { get; set; }
    }
}
