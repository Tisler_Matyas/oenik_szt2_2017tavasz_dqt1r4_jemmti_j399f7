﻿// <copyright file="UserViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat.VM
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Converters;
    using data;
    using data.Interface;
    using data.Repositories;
    using logic.LogicClasses;

    public class UserViewModel : BaseVM
    {
        private static UserViewModel theUserViewModel;
        private IUserOperator userOperator;
        private ObservableCollection<ITransaction> transactions;
        private BalanceHistoryVM payment;
        private Guid userID;
        private string username;
        private ProductBrowsingVM ownedProducts;
        private ProductBrowsingVM allProducts;
        private int userBalance;

        private UserViewModel()
        {
            BalanceRepository bRepo = new BalanceRepository();
            UserRepository uRepo = new UserRepository();
            ProductRepository pRepo = new ProductRepository();
            HistoryRepository hRepo = new HistoryRepository();
            CategoryRepository cRepo = new CategoryRepository();
            this.userOperator = new UserOperatorLogic(bRepo, uRepo, cRepo, pRepo, hRepo);
            this.allProducts = new ProductBrowsingVM(false);
            this.ownedProducts = new ProductBrowsingVM(true);
            this.Refresh();
        }

        public static UserViewModel TheUserViewModel
        {
            get
            {
                if (theUserViewModel == null)
                {
                    theUserViewModel = new UserViewModel();
                }

                return theUserViewModel;
            }
        }

        public ProductBrowsingVM OwnedProducts
        {
            get { return this.ownedProducts; }
            set { this.SetProperty(ref this.ownedProducts, value); }
        }

        public ProductBrowsingVM AllProducts
        {
            get { return this.allProducts; }
            set { this.SetProperty(ref this.allProducts, value); }
        }

        // private ProductVM selectedProduct;
        // private ProductVM selectedOwnedProduct;
        // private ObservableCollection<ProductVM> ownedProducts;
        // private ObservableCollection<ProductVM> allProducts;
        // private ObservableCollection<ProductVM> filteredProducts;
        // private ObservableCollection<ProductVM> filteredOwnedProducts;

        // public ObservableCollection<ProductVM> FilteredOwnedProducts
        // {
        //    get { return this.filteredOwnedProducts; }
        //    set { this.SetProperty(ref this.filteredOwnedProducts, value); }
        // }

        // public ProductVM SelectedProduct
        // {
        //    get { return this.selectedProduct; }
        //    set { this.SetProperty(ref this.selectedProduct, value); }
        // }

        // public ObservableCollection<ProductVM> AllProducts
        // {
        //    get { return this.allProducts; }
        //    set { this.SetProperty(ref this.allProducts, value); }
        // }

        // public ObservableCollection<ProductVM> FilteredProducts
        // {
        //    get { return this.filteredProducts; }
        //    set { this.SetProperty(ref this.filteredProducts, value); }
        // }
        public string Username
        {
            get
            {
                if (this.username == null)
                {
                    this.username = this.userOperator.UserNevLekeres(this.UserID);
                }

                return this.username;
            }
        }

        public int UserBalance
        {
            get { return this.userBalance; }
            set { this.SetProperty(ref this.userBalance, value); }
        }

        public BalanceHistoryVM Payment
        {
            get { return this.payment; }
            set { this.SetProperty(ref this.payment, value); }
        }

        public ObservableCollection<ITransaction> Transactions
        {
            get { return this.transactions; }
            set { this.SetProperty(ref this.transactions, value); }
        }

        // public ProductVM SelectedOwnedProduct
        // {
        //    get { return selectedOwnedProduct; }
        //    set { SetProperty(ref this.selectedOwnedProduct, value); }
        // }

        // public ObservableCollection<ProductVM> OwnedProducts
        // {
        //    get { return ownedProducts; }
        //    set { SetProperty(ref this.ownedProducts, value); }
        // }

        // public ProductVM SelectedProduct
        // {
        //    get { return selectedProduct; }
        //    set { SetProperty(ref this.selectedProduct, value); }
        // }

        // public ObservableCollection<ProductVM> AllProducts
        // {
        //    get { return this.allProducts; }
        //    set { this.SetProperty(ref this.allProducts, value); }
        // }
        public Guid UserID
        {
            get { return this.userID; }
            set { this.SetProperty(ref this.userID, value); }
        }

        /// <summary>
        /// If a DB change were made, VM fields get the newer values.
        /// </summary>
        public void Refresh()
        {
            this.AllProducts.AllProducts = new ObservableCollection<ProductVM>(this.userOperator.GetProducts().Select(x => ProductToProductVMConverter.Convert(x)));
            this.AllProducts.FilteredProducts = this.AllProducts.AllProducts;
            this.AllProducts.Categories = this.userOperator.KategoriaLekeres().Select(x => x.Name).ToList();

            this.OwnedProducts.FilteredProducts = this.OwnedProducts.AllProducts;
            this.OwnedProducts.Categories = this.userOperator.KategoriaLekeres().Select(x => x.Name).ToList();

            if (this.userID != Guid.Empty)
            {
                this.OwnedProducts.AllProducts = new ObservableCollection<ProductVM>(this.userOperator.TartalomLekeres(this.userID).Select(x => ProductToProductVMConverter.Convert(x)));
                this.UserBalance = this.userOperator.EgyenlegLekeres(this.UserID);
                this.Transactions = new ObservableCollection<ITransaction>(this.userOperator.TranzakcioLekeres(this.userID));
            }
        }
    }
}
