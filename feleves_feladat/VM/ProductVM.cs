﻿// <copyright file="ProductVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System;
    using System.Collections.ObjectModel;

    [Serializable]
    public class ProductVM : BaseVM
    {
        private Guid id;
        private string title;
        private int releaseYear;
        private double iMDB;
        private string director;
        private int runTime;
        private string category;
        private int price;
        private string description;
        private string contentURL;
        private ObservableCollection<string> categories;

        public ProductVM()
        {
            this.Categories = new ObservableCollection<string>();
        }

        public ProductVM(
         Guid id, string title = null, int releaseYear = 0, double iMDB = 0, string categoryName = null, int price = 0, string description = null, string contentURL = null, string director = null, int runTime = 0)
        {
            this.id = id;
            this.title = title;
            this.releaseYear = releaseYear;
            this.iMDB = iMDB;
            this.director = director;
            this.runTime = runTime;
            this.description = description;
            this.contentURL = contentURL;
            this.Category = categoryName;
            this.Price = price;
        }

        public ObservableCollection<string> Categories
        {
            get { return this.categories; }
            set { this.SetProperty(ref this.categories, value); }
        }

        public string ContentURL
        {
            get { return this.contentURL; }
            set { this.contentURL = value; }
        }

        public string Title
        {
            get { return this.title; }
            set { this.SetProperty(ref this.title, value); }
        }

        public int ReleaseYear
        {
            get { return this.releaseYear; }
            set { this.SetProperty(ref this.releaseYear, value); }
        }

        public double IMDB
        {
            get { return this.iMDB; }
            set { this.SetProperty(ref this.iMDB, value); }
        }

        public string Director
        {
            get { return this.director; }
            set { this.SetProperty(ref this.director, value); }
        }

        public int RunTime
        {
            get { return this.runTime; }
            set { this.SetProperty(ref this.runTime, value); }
        }

        public string Description
        {
            get { return this.description; }
            set { this.SetProperty(ref this.description, value); }
        }

        public Guid Id
        {
            get { return this.id; }
            private set { this.id = value; }
        }

        public string Category
        {
            get { return this.category; }
            set { this.SetProperty(ref this.category, value); }
        }

        public int Price
        {
            get { return this.price; }
            set { this.SetProperty(ref this.price, value); }
        }

        internal void CopyFromClone(ProductVM clone)
        {
            this.Id = clone.Id;
            this.Title = clone.Title;
            this.IMDB = clone.IMDB;
            this.ReleaseYear = clone.ReleaseYear;
            this.RunTime = clone.RunTime;
            this.Category = clone.Category;
            this.Price = clone.Price;
        }
    }
}
