﻿// <copyright file="AdminProductAddModWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for AdminProductAddMod.xaml
    /// </summary>
    public partial class AdminProductAddMod : Window
    {
        public AdminProductAddMod()
        {
            this.InitializeComponent();
        }

        public AdminProductAddMod(ProductVM pVM)
        {
            this.InitializeComponent();
            this.DataContext = pVM;
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (!((e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || (e.Key == Key.Delete) || (e.Key == Key.Back)))
            {
                e.Handled = true;
            }
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
