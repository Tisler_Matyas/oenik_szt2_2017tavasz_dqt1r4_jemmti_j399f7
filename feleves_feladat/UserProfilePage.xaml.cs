﻿// <copyright file="UserProfilePage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System.Windows;
    using System.Windows.Controls;
    using feleves_feladat.VM;

    /// <summary>
    /// Interaction logic for Profile.xaml
    /// </summary>
    public partial class UserProfilePage : Page
    {
        private UserViewModel userVM;

        public UserProfilePage()
        {
            this.InitializeComponent();
            this.userVM = UserViewModel.TheUserViewModel;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this.userVM;
        }
    }
}
