﻿// <copyright file="UserProductProfileWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System;
    using System.Windows;
    using data;
    using data.Repositories;
    using Exceptions;
    using feleves_feladat.VM;
    using logic.LogicClasses;

    /// <summary>
    /// Interaction logic for UserProductProfileWindow.xaml
    /// </summary>
    public partial class UserProductProfileWindow : Window
    {
        private UserViewModel userVM;
        private bool isOwned;
        private IUserOperator userOperator;

        public UserProductProfileWindow(bool isOwned)
        {
            this.InitializeComponent();
            this.isOwned = isOwned;
            BalanceRepository bRepo = new BalanceRepository();
            UserRepository uRepo = new UserRepository();
            ProductRepository pRepo = new ProductRepository();
            HistoryRepository hRepo = new HistoryRepository();
            CategoryRepository cRepo = new CategoryRepository();
            this.userOperator = new UserOperatorLogic(bRepo, uRepo, cRepo, pRepo, hRepo);
            this.userVM = UserViewModel.TheUserViewModel;
            this.DataContext = isOwned ? this.userVM.OwnedProducts.SelectedProduct : this.userVM.AllProducts.SelectedProduct;
        }

        private void Buy_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.userOperator.TartalomVasarlas(
                    this.userVM.UserID,
                    this.isOwned ? this.userVM.OwnedProducts.SelectedProduct.Id : this.userVM.AllProducts.SelectedProduct.Id);
            }
            catch (UserNotExistsException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (UserHasNotEnoughMoneyException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            this.userVM.Refresh();
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // this.DataContext = this.userVM.SelectedProduct;
        }
    }
}
