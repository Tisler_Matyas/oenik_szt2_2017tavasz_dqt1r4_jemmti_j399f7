﻿// <copyright file="UserPaymentPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using data;
    using data.Repositories;
    using feleves_feladat.VM;
    using logic.LogicClasses;

    /// <summary>
    /// Interaction logic for Payment.xaml
    /// </summary>
    public partial class UserPaymentPage : Page
    {
        private UserViewModel userVM;
        private IUserOperator userOperator;

        public UserPaymentPage()
        {
            this.InitializeComponent();

            BalanceRepository bRepo = new BalanceRepository();
            UserRepository uRepo = new UserRepository();
            ProductRepository pRepo = new ProductRepository();
            HistoryRepository hRepo = new HistoryRepository();
            CategoryRepository cRepo = new CategoryRepository();
            this.userOperator = new UserOperatorLogic(bRepo, uRepo, cRepo, pRepo, hRepo);
            this.userVM = UserViewModel.TheUserViewModel;
            this.userVM.Payment = new BalanceHistoryVM(Guid.NewGuid(), this.userVM.UserID, DateTime.Now, 0);
            this.DataContext = this.userVM.Payment;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            if (this.userVM.Payment != null)
            {
                this.userOperator.NewBalanceHistory(this.userVM.Payment.UserID, this.userVM.Payment.Price, this.userVM.Payment.OrderDate);
                MessageBox.Show("Transaction submitted");
                this.userVM.Refresh();
            }
        }

        private void OnlyNumbers_PrewiewKexdown(object sender, KeyEventArgs e)
        {
            if (!((e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || (e.Key == Key.Delete) || (e.Key == Key.Back)))
            {
                e.Handled = true;
            }
        }
    }
}
