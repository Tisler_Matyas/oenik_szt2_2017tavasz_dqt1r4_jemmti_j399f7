﻿// <copyright file="ProductToProductVMConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat.Converters
{
    using data.Model;

    public static class ProductToProductVMConverter
    {
        public static ProductVM Convert(object value)
        {
            if (value is Product)
            {
                Product p = value as Product;
                int runtime = p.Runtime == null ? 0 : (int)p.Runtime;
                ProductVM pvm = new ProductVM(p.ProductID, p.Title, (int)p.ReleaseYear, (int)p.IMDB, p.Category1.Name, (int)p.Price, p.Description, p.ContentUrl, p.Director, runtime);
                return pvm;
            }
            else
            {
                return null;
            }
        }

        public static Product ConvertBack(object value)
        {
            if (value is ProductVM)
            {
                ProductVM pvm = value as ProductVM;
                Product p = new Product
                {
                    ProductID = pvm.Id,
                    Category = pvm.Category,
                    Title = pvm.Title,
                    IMDB = (decimal?)pvm.IMDB,
                    Price = pvm.Price,
                    Description = pvm.Description,
                    ReleaseYear = pvm.ReleaseYear,
                    Director = pvm.Director,
                    Runtime = pvm.RunTime,
                    ContentUrl = pvm.ContentURL
                };
                return p;
            }
            else
            {
                return null;
            }
        }
    }
}
