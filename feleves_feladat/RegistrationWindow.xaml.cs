﻿// <copyright file="RegistrationWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System;
    using System.Windows;
    using data;
    using Exceptions;
    using logic.LogicClasses;

    /// <summary>
    /// Interaction logic for RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        private UserVM regVM;
        private IRegistration regOperator;

        public RegistrationWindow()
        {
            this.InitializeComponent();
        }

        private void RegistrationClick(object sender, RoutedEventArgs e)
        {
            try
            {
                this.regOperator.Registration(this.regVM.Fullname, this.regVM.Username, this.regVM.Email, this.regVM.FirstPassword);
                this.DialogResult = true;
                MessageBox.Show("Registration OK");
                this.Close();
            }
            catch (EmailExistsException em)
            {
                MessageBox.Show(em.Message);
                this.DialogResult = false;
            }
            catch (UsernameExistsException em)
            {
                MessageBox.Show(em.Message);
                this.DialogResult = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.regVM = new UserVM();
            UserRepository uRepo = new UserRepository();
            this.regOperator = new RegistrationLogic(uRepo);
            this.DataContext = this.regVM;
        }
    }
}
