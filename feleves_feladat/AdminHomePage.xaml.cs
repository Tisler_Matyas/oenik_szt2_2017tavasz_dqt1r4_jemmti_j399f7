﻿// <copyright file="AdminHomePage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System.Windows.Controls;
    using data;
    using data.Repositories;
    using logic.LogicClasses;

    /// <summary>
    /// Interaction logic for AdminHomePage.xaml
    /// </summary>
    public partial class AdminHomePage : Page
    {
        private AdminViewModel adminVM;

        public AdminHomePage()
        {
            this.InitializeComponent();
            this.adminVM = AdminViewModel.AdminVM;
            BalanceRepository bRepo = new BalanceRepository();
            UserRepository uRepo = new UserRepository();
            ProductRepository pRepo = new ProductRepository();
            HistoryRepository hRepo = new HistoryRepository();
            CategoryRepository cRepo = new CategoryRepository();
            UserOperatorLogic userOperator = new UserOperatorLogic(bRepo, uRepo, cRepo, pRepo, hRepo);
            this.DataContext = userOperator.UserNevLekeres(this.adminVM.UserID);
        }
    }
}
