﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System;
    using System.Security.Authentication;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using data;
    using data.Repositories;
    using Exceptions;
    using logic.Interfaces;
    using logic.LogicClasses;
    using VM;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IUserOperator userOperator;
        private IAdminOperator adminOperator;
        private MainWindowViewModel mainWinVM;

        public MainWindow()
        {
            this.InitializeComponent();
            this.mainWinVM = new MainWindowViewModel();
            BalanceRepository bRepo = new BalanceRepository();
            UserRepository uRepo = new UserRepository();
            ProductRepository pRepo = new ProductRepository();
            HistoryRepository hRepo = new HistoryRepository();
            CategoryRepository cRepo = new CategoryRepository();
            this.userOperator = new UserOperatorLogic(bRepo, uRepo, cRepo, pRepo, hRepo);
            this.adminOperator = new AdminOperatorLogic(uRepo, pRepo, bRepo, cRepo);
            this.DataContext = this.mainWinVM;
        }

        private void Label_PreviewMousedownRegister(object sender, MouseButtonEventArgs e)
        {
            RegistrationWindow regwin = new RegistrationWindow();
            regwin.ShowDialog();
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            Guid userID;

            try
            {
                // I know its ugly :(
                string password = new System.Net.NetworkCredential(string.Empty, this.mainWinVM.Password).Password;

                userID = this.userOperator.UserLekeres(this.mainWinVM.UserName, password);
                UserViewModel uVM = UserViewModel.TheUserViewModel;
                AdminViewModel adminVM = AdminViewModel.AdminVM;
                adminVM.UserID = userID;
                uVM.UserID = userID;
                adminVM.Refresh();
                uVM.Refresh();
                if (this.adminOperator.AdminE(userID))
                {
                    AdminHomeWindow aHomeWin = new AdminHomeWindow(userID);
                    aHomeWin.Show();
                }
                else
                {
                    UserHomeWindow uHomeWin = new UserHomeWindow(userID);
                    uHomeWin.Show();
                }

                this.Close();
            }
            catch (AuthenticationException noUser)
            {
                MessageBox.Show(noUser.Message);
            }
            catch (UserNotExistsException noUser)
            {
                MessageBox.Show(noUser.Message);
            }
        }

        private void TxtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            {
                ((dynamic)this.DataContext).Password = ((PasswordBox)sender).SecurePassword;
            }
        }
    }
}
