﻿// <copyright file="AdminHomeWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System;
    using System.Windows;

    /// <summary>
    /// Interaction logic for AdminHomeWindow.xaml
    /// </summary>
    public partial class AdminHomeWindow : Window
    {
        private AdminTransactionManagerPage adminTransactionManager;
        private AdminProductManagerPage adminProductManager;
        private AdminViewModel adminVM;

        public AdminHomeWindow()
        {
            this.InitializeComponent();
        }

        public AdminHomeWindow(Guid userID)
        {
            this.InitializeComponent();
            this.adminVM = AdminViewModel.AdminVM;
            this.adminVM.UserID = userID;
        }

        private void ProductsClick(object sender, RoutedEventArgs e)
        {
            this.AdminHomeFrame.Content = this.adminProductManager;
        }

        private void TransactionsClick(object sender, RoutedEventArgs e)
        {
            this.AdminHomeFrame.Content = this.adminTransactionManager;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.adminProductManager = new AdminProductManagerPage();
            this.adminTransactionManager = new AdminTransactionManagerPage();
            this.AdminHomeFrame.Content = new AdminHomePage();
        }

        private void SignOut_Click(object sender, RoutedEventArgs e)
        {
            MainWindow adWin = new MainWindow();
            adWin.Show();
            this.Close();
        }
    }
}
