﻿// <copyright file="UserHomeWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System;
    using System.Windows;
    using VM;

    /// <summary>
    /// Interaction logic for UserHomeWindow.xaml
    /// </summary>
    public partial class UserHomeWindow : Window
    {
        private UserViewModel VM;
        private UserBrowsingVideosPage userBrowsingAllVideosPage;
        private UserPaymentPage userPaymentPage;
        private UserProfilePage userProfilePage;
        private UserBrowsingVideosPage userBrowsingOwnedVideosPage;

        public UserHomeWindow()
        {
            this.InitializeComponent();
        }

        public UserHomeWindow(Guid userID)
        {
            this.InitializeComponent();
            this.VM = UserViewModel.TheUserViewModel;
            this.VM.UserID = userID;
            this.userBrowsingAllVideosPage = new UserBrowsingVideosPage(this.VM.AllProducts);
            this.userBrowsingOwnedVideosPage = new UserBrowsingVideosPage(this.VM.OwnedProducts);
            this.userProfilePage = new UserProfilePage();
            this.userPaymentPage = new UserPaymentPage();
            this.HomeFrame.Content = this.userBrowsingAllVideosPage;
        }

        private void BrowsingClick(object sender, RoutedEventArgs e)
        {
            this.HomeFrame.Content = this.userBrowsingAllVideosPage;
        }

        private void MyVideosClick(object sender, RoutedEventArgs e)
        {
            this.HomeFrame.Content = this.userBrowsingOwnedVideosPage;
        }

        private void ProfileClick(object sender, RoutedEventArgs e)
        {
            this.HomeFrame.Content = this.userProfilePage;
        }

        private void PaymentClick(object sender, RoutedEventArgs e)
        {
            this.HomeFrame.Content = this.userPaymentPage;
        }

        private void SignOutClick(object sender, RoutedEventArgs e)
        {
            MainWindow win = new MainWindow();
            win.Show();
            this.Close();
        }
    }
}
