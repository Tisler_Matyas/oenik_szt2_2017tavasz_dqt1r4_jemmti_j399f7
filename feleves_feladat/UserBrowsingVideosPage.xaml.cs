﻿// <copyright file="UserBrowsingVideosPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Controls;
    using Converters;
    using data;
    using data.Repositories;
    using feleves_feladat.VM;
    using logic.LogicClasses;

    /// <summary>
    /// Interaction logic for BrowsingVideos.xaml
    /// </summary>
    public partial class UserBrowsingVideosPage : Page
    {
        private UserViewModel vm;
        private IUserOperator userOperator;
        private ProductBrowsingVM prodBrowsVM;

        public UserBrowsingVideosPage(ProductBrowsingVM pbVM)
        {
            this.InitializeComponent();
            this.vm = UserViewModel.TheUserViewModel;
            this.prodBrowsVM = pbVM;
            BalanceRepository bRepo = new BalanceRepository();
            UserRepository uRepo = new UserRepository();
            ProductRepository pRepo = new ProductRepository();
            HistoryRepository hRepo = new HistoryRepository();
            CategoryRepository cRepo = new CategoryRepository();
            this.userOperator = new UserOperatorLogic(bRepo, uRepo, cRepo, pRepo, hRepo);
            this.DataContext = this.prodBrowsVM;
            this.title.Content = this.prodBrowsVM.IsOwned ? "My videos :)" : "New videos to buy";
        }

        private void ListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.prodBrowsVM.SelectedProduct != null)
            {
                UserProductProfileWindow prodWin = new UserProductProfileWindow(this.prodBrowsVM.IsOwned);
                prodWin.ShowDialog();
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.prodBrowsVM.SelectedCategory != null)
            {
                if (this.prodBrowsVM.IsOwned)
                {
                    this.prodBrowsVM.FilteredProducts = new ObservableCollection<ProductVM>(
                    this.userOperator.FilteredProducts(this.prodBrowsVM.SelectedCategory, this.vm.UserID)
                    .Select(x => ProductToProductVMConverter.Convert(x)));
                }
                else
                {
                    this.prodBrowsVM.FilteredProducts = new ObservableCollection<ProductVM>(
                    this.userOperator.FilteredProducts(this.prodBrowsVM.SelectedCategory)
                    .Select(x => ProductToProductVMConverter.Convert(x)));
                }
            }
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.prodBrowsVM.FilteredProducts = this.prodBrowsVM.AllProducts;
        }
    }
}
