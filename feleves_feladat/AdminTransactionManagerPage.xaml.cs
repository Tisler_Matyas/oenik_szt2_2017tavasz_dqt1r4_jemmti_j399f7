﻿// <copyright file="AdminTransactionManagerPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System.Windows;
    using System.Windows.Controls;
    using data;
    using data.Repositories;
    using logic.Interfaces;
    using logic.LogicClasses;

    /// <summary>
    /// Interaction logic for AdminTransactionManagerPage.xaml
    /// </summary>
    public partial class AdminTransactionManagerPage : Page
    {
        private AdminViewModel adminViewModel;
        private IAdminOperator adminOp;

        public AdminTransactionManagerPage()
        {
            this.InitializeComponent();
            this.adminViewModel = AdminViewModel.AdminVM;
            this.DataContext = this.adminViewModel;
            BalanceRepository bRepo = new BalanceRepository();
            UserRepository uRepo = new UserRepository();
            ProductRepository pRepo = new ProductRepository();
            CategoryRepository cRepo = new CategoryRepository();
            this.adminOp = new AdminOperatorLogic(uRepo, pRepo, bRepo, cRepo);
        }

        private void Approve_Click(object sender, RoutedEventArgs e)
        {
            if (this.adminViewModel.SelectedTransaction != null)
            {
                this.adminOp.VerifyBalanceTransaction(this.adminViewModel.SelectedTransaction.ID, true);
                this.adminViewModel.Refresh();
            }
        }

        // private void Decline_Click(object sender, RoutedEventArgs e)
        // {
        //    if (this.adminViewModel.SelectedTransaction != null)
        //    {
        //        this.adminOp.VerifyBalanceTransaction(this.adminViewModel.SelectedTransaction.ID, false);
        //        this.adminViewModel.Refresh();
        //    }
        // }
    }
}
