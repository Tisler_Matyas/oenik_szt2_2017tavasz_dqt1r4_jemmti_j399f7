﻿// <copyright file="AdminProductManagerPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System.Windows;
    using System.Windows.Controls;
    using data;
    using data.Repositories;
    using feleves_feladat.Converters;
    using logic.Interfaces;
    using logic.LogicClasses;

    /// <summary>
    /// Interaction logic for AdminProductManager.xaml
    /// </summary>
    public partial class AdminProductManagerPage : Page
    {
        private IAdminOperator adminLogic;
        private IUserOperator userLogic;
        private AdminViewModel VM;

        public AdminProductManagerPage()
        {
            this.InitializeComponent();
            BalanceRepository bRepo = new BalanceRepository();
            UserRepository uRepo = new UserRepository();
            ProductRepository pRepo = new ProductRepository();
            HistoryRepository hRepo = new HistoryRepository();
            CategoryRepository cRepo = new CategoryRepository();
            this.userLogic = new UserOperatorLogic(bRepo, uRepo, cRepo, pRepo, hRepo);
            this.adminLogic = new AdminOperatorLogic(uRepo, pRepo, bRepo, cRepo);
        }

        private void ProductModClick(object sender, RoutedEventArgs e)
        {
            if (this.VM.SelectedProduct == null)
            {
                return;
            }

            ProductVM clone = BaseVM.DeepClone(this.VM.SelectedProduct);
            clone.Categories = this.VM.Categories;
            AdminProductAddMod modwin = new AdminProductAddMod(clone);
            if (modwin.ShowDialog() == true)
            {
                this.VM.SelectedProduct.CopyFromClone(clone);
                this.adminLogic.ModProduct(ProductToProductVMConverter.ConvertBack(this.VM.SelectedProduct));
                this.VM.Refresh();
                MessageBox.Show("Mod OK");
            }
            else
            {
                MessageBox.Show("Cancelled");
            }
        }

        private void ProductAddClick(object sender, RoutedEventArgs e)
        {
            ProductVM newProd = new ProductVM();
            newProd.Categories = this.VM.Categories;
            AdminProductAddMod addwin = new AdminProductAddMod(newProd);
            if (addwin.ShowDialog() == true)
            {
                this.adminLogic.SaveProduct(ProductToProductVMConverter.ConvertBack(newProd));
                this.VM.Refresh();
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            this.VM = AdminViewModel.AdminVM;
            this.DataContext = this.VM;
        }
    }
}
