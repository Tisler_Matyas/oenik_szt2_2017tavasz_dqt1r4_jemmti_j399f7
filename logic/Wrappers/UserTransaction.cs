﻿// <copyright file="UserTransaction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace logic.Wrappers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using data.Interface;

    public class UserTransaction : ITransaction
    {
        public string Description
        {
            get;
            set;
        }

        public DateTime OrderDate
        {
            get;
            set;
        }

        public int Price
        {
            get;
            set;
        }

        public string ProductName
        {
            get;
            set;
        }
    }
}
