﻿// <copyright file="ITransaction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace data.Interface
{
    using System;

    // a balancehistory és history "hasonló" oszlopaiból, hogy a user számára együtt lehessen listázni
    public interface ITransaction
    {
        DateTime OrderDate { get; set; }

        // sima history: filmcím
        string ProductName { get; set; }

        // balancehistory: mindig ua.pl.: "Account transaction"
        string Description { get; set; }

        // sima history: a film ára
        // balancehistory: price
        int Price { get; set; }
    }
}
