﻿// <copyright file="IAdminOperator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using data.Model;

    public interface IAdminOperator
    {
        /// <summary>
        /// Specifies the user's authority.
        /// </summary>
        /// <param name="userID">User's ID</param>
        /// <returns>True, if the user is Administrator.</returns>
        bool AdminE(Guid userID);

        /// <summary>
        /// All payment requests.
        /// </summary>
        /// <returns>Payments waiting to be approved</returns>
        List<BalanceHistory> PaymentApprovementRequests();

        /// <summary>
        /// All products
        /// </summary>
        /// <returns>All products from data</returns>
        List<Product> AllProducts();

        /// <summary>
        /// Add one product.
        /// </summary>
        /// <param name="newProduct">Product to be saved.</param>
        void SaveProduct(Product newProduct);

        /// <summary>
        /// Update one Product.
        /// </summary>
        /// <param name="modProduct">Product to be updated.</param>
        void ModProduct(Product modProduct);

        /// <summary>
        /// User balanceHistory approving, and user balanace update
        /// </summary>
        /// <param name="transactionID">which transaction</param>
        /// <param name="verified">true if verifying</param>
        void VerifyBalanceTransaction(Guid transactionID, bool verified);

        // +++++Optional: product title and sum of income from sold products, to draw graphs to adminhomepage
        // Dictionary<string,int> CorporationIncome();
    }
}
