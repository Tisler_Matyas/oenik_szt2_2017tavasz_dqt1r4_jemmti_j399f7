﻿// <copyright file="IUserOperator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace feleves_feladat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using data;
    using data.Interface;
    using data.Model;
    using logic;

    public interface IUserOperator
    {
        // User ID-t fog visszaadni - belépéskor hivjuk meg
        // ha nincs ilyen User, akkor vmi exception
        Guid UserLekeres(string name, string password);

        List<ITransaction> TranzakcioLekeres(Guid userID);

        void NewBalanceHistory(Guid userID, int money_amount, DateTime time, string card_number = null, string full_name = "");

        // List<Category> -t ad vissza
        List<Category> KategoriaLekeres();

        // List<Tartalom> ad vissza, ami megvan vásárolva a usernek
        List<Product> TartalomLekeres(Guid userID);

        /// <summary>
        /// Returns all products from Data.
        /// </summary>
        /// <returns>List of products.</returns>
        List<Product> GetProducts();

        int EgyenlegLekeres(Guid userID);

        string UserNevLekeres(Guid userID);

        // Tartalmat ad még paraméternél --> igy lehet ellenorizni hogy van e elég egyenleg
        // bool EgyenlegEllenorzes(string userID, Product product);

        // megvásárolt tartalmat adja vissza, miután az árát levonta a user egyenlegéről, ha nincs ennyi pénze => Exception
        void TartalomVasarlas(Guid userID, Guid productID);

        // azok a filmek amiknek ez a kategóriája és ez a felhasználó már megvette
        List<Product> FilteredProducts(string categoryName, Guid userID);

        // azok a filmek amiknek ez a kategóriája
        List<Product> FilteredProducts(string categoryName);
    }
}
