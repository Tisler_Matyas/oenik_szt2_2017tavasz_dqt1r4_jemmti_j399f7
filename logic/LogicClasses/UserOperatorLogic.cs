﻿// <copyright file="UserOperatorLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace logic.LogicClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Authentication;
    using System.Security.Cryptography;
    using data.Interface;
    using data.Model;
    using feleves_feladat;
    using global::Exceptions;
    using Helpers;
    using Wrappers;

    public class UserOperatorLogic : IUserOperator
    {
        private IBalance balanceRepository;
        private IUser userRepository;
        private ICategory categoryRepository;
        private IProduct productRepository;
        private IHistory historyRepository;

        public UserOperatorLogic(IBalance pBalanceRepository, IUser pUserRepository, ICategory pCategoryRepository, IProduct pProductRepository, IHistory pHistoryRepository)
        {
            this.balanceRepository = pBalanceRepository;
            this.userRepository = pUserRepository;
            this.categoryRepository = pCategoryRepository;
            this.productRepository = pProductRepository;
            this.historyRepository = pHistoryRepository;
        }

        public int EgyenlegLekeres(Guid userID)
        {
            var user = this.userRepository.GetAll().FirstOrDefault(x => x.UserID == userID);

            if (user == null)
            {
                throw new UserNotExistsException("The user doesn't exist in the system");
            }

            return (int)user.Balance;
        }

        public List<Product> FilteredProducts(string categoryName)
        {
            var category = this.categoryRepository.GetAll().First(x => x.Name == categoryName);

            if (category == null)
            {
                throw new CategoryNotExistsException("This category doesn't exist in the system");
            }

            return this.productRepository.GetAll().Where(x => x.CategoryID == category.CategoryID).ToList();
        }

        public List<Product> FilteredProducts(string categoryName, Guid userID)
        {
            var productIDs = this.historyRepository.GetAll().Where(x => x.UserID == userID).Select(x => x.ProductID).ToList();

            var category = this.categoryRepository.GetAll().First(x => x.Name == categoryName);

            if (category == null)
            {
                throw new CategoryNotExistsException("This category doesn't exist in the system");
            }

            return this.productRepository.GetAll().Where(x => x.CategoryID == category.CategoryID
                                                            && productIDs.Contains(x.ProductID)).ToList();
        }

        public List<Category> KategoriaLekeres()
        {
            return this.categoryRepository.GetAll().ToList();
        }

        public void NewBalanceHistory(Guid userID, int money_amount, DateTime time, string card_number = null, string full_name = "")
        {
            BalanceHistory balance = new BalanceHistory();

            balance.UserID = userID;
            balance.Price = money_amount;
            balance.OrderDate = time;
            balance.IsApproved = false;

            this.balanceRepository.Save(balance);
        }

        public List<Product> TartalomLekeres(Guid userID)
        {
            // Get the user's product ids from the history
            var productIDs = this.historyRepository.GetAll().Where(x => x.UserID == userID).Select(x => x.ProductID).ToList();

            // Return all of the products, what are in the last id list
            if (productIDs != null)
            {
                return this.productRepository.GetAll().Where(x => productIDs.Contains(x.ProductID)).ToList();
            }
            else
            {
                return null;
            }
        }

        public void TartalomVasarlas(Guid userID, Guid productID)
        {
            // Get the current product
            var product = this.productRepository.GetAll().First(x => x.ProductID == productID);

            if (product == null)
            {
                throw new Exception("Product doesn't exist in the system");
            }

            // Get the current user
            var user = this.userRepository.GetAll().First(x => x.UserID == userID);

            if (user == null)
            {
                throw new UserNotExistsException("User doesn't exist in the database");
            }

            // Check the balance of the user
            if (user.Balance < product.Price)
            {
                throw new UserHasNotEnoughMoneyException("User hasn't got enough money for this product");
            }

            var ownedProducts = this.TartalomLekeres(userID);
            if (ownedProducts.Select(x => x.ProductID).Contains(productID))
            {
                throw new UserHaveThisAlreadyException("User have this product already!");
            }

            // Reduce the balance
            user.Balance -= product.Price;

            History history = new History();
            history.OrderDate = DateTime.Now;
            history.Price = product.Price;
            history.ProductID = product.ProductID;
            history.UserID = user.UserID;

            // Save the user
            this.userRepository.Modify(user);

            // Add the history
            this.historyRepository.Save(history);
        }

        /// <summary>
        /// Merge user's BalanceHistory and History elements.
        /// </summary>
        ///
        /// <returns>Transaction list</returns><param name="userID">User's ID./param>
        /// <returns>User's account changes.</returns>
        public List<ITransaction> TranzakcioLekeres(Guid userID)
        {
            List<ITransaction> result = new List<ITransaction>();

            // Get all products
            var products = this.productRepository.GetAll();

            // Get the balance history of the user
            var balanceHistory = this.balanceRepository.GetAll().Where(x => x.UserID == userID);

            // Fill transaction elements from balanc history
            foreach (var element in balanceHistory)
            {
                UserTransaction resultElement = new UserTransaction();
                resultElement.Description = "Account transaction";
                resultElement.OrderDate = (DateTime)element.OrderDate;
                resultElement.Price = (int)element.Price;
                resultElement.ProductName = string.Empty;

                result.Add(resultElement);
            }

            // Get the purchase history of the user
            var history = this.historyRepository.GetAll().Where(x => x.UserID == userID);

            foreach (var element in history)
            {
                UserTransaction resultElement = new UserTransaction();
                resultElement.Description = "Product purchase: " + element.Product.Title;
                resultElement.OrderDate = (DateTime)element.OrderDate;
                resultElement.Price = (int)element.Price * -1;
                resultElement.ProductName = products.First(x => x.ProductID == element.ProductID).Title;

                result.Add(resultElement);
            }

            return result;
        }

        public Guid UserLekeres(string name, string password)
        {
            User user;

            // Get the user by username
            try
            {
                user = this.userRepository.GetAll()?.First(x => x.Username == name);
            }
            catch (InvalidOperationException)
            {
                throw new AuthenticationException("Your login data is corrupted, please try with the right username and password!");
            }

            // throw exception if it's not exists
            if (user == null)
            {
                throw new AuthenticationException("Your login data is corrupted, please try with the right username and password!");
            }

            // create hash from the password
            string passwordHash = PasswordHelper.ComputeHash(password, new SHA256CryptoServiceProvider());

            // if it's not equals with the user's hash, throw exception
            if (passwordHash != user.Password)
            {
                throw new AuthenticationException("Your login data is corrupted, please try with the right username and password!");
            }

            // return the id of the user
            return user.UserID;
        }

        public string UserNevLekeres(Guid userID)
        {
            return this.userRepository.GetAll().First(x => x.UserID == userID).FullName;
        }

        public List<Product> GetProducts()
        {
            return this.productRepository.GetAll().ToList();
        }
    }
}
