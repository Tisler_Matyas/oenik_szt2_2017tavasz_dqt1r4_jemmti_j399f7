﻿// <copyright file="AdminOperatorLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace logic.LogicClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using data.Interface;
    using data.Model;
    using Exceptions;
    using global::Exceptions;
    using logic.Interfaces;

    public class AdminOperatorLogic : IAdminOperator
    {
        private IUser userRepository;
        private IProduct productRepository;
        private IBalance balanceRepository;
        private ICategory categoryRepository;

        public AdminOperatorLogic(IUser pUserRepository, IProduct pProductRepository, IBalance pBalanceRepository, ICategory pCategoryRepository)
        {
            this.categoryRepository = pCategoryRepository;
            this.userRepository = pUserRepository;
            this.productRepository = pProductRepository;
            this.balanceRepository = pBalanceRepository;
        }

        public List<Product> AllProducts()
        {
            return this.productRepository.GetAll().ToList();
        }

        public void ModProduct(Product modProduct)
        {
            modProduct.CategoryID = this.GetCategoryID(modProduct.Category);
            this.productRepository.Modify(modProduct);
        }

        public List<BalanceHistory> PaymentApprovementRequests()
        {
            return this.balanceRepository.GetNotApprovedHistory().ToList();
        }

        public void SaveProduct(Product newProduct)
        {
            newProduct.CategoryID = this.GetCategoryID(newProduct.Category);
            this.productRepository.Save(newProduct);
        }

        public void VerifyBalanceTransaction(Guid transactionID, bool verified)
        {
            // Get the verified transaction
            var transaction = this.balanceRepository.GetAll().First(x => x.BalanceHistoryID == transactionID);

            if (transaction != null)
            {
                // TODO: need to change it to enum
                // update the approved flag
                transaction.IsApproved = verified;

                // Get the user instance
                var user = this.userRepository.GetAll().First(x => x.UserID == transaction.UserID);

                if (user == null)
                {
                    throw new UserNotExistsException("The user is not exists in the system");
                }

                // If the transaction is verified, update the balance of the user
                if (verified)
                {
                    user.Balance += transaction.Price;

                    this.userRepository.Modify(user);
                }

                // Save the balance history
                this.balanceRepository.Modify(transaction);
            }
            else
            {
                throw new BalanceNotExistsException("The given transaction not exists in the system");
            }
        }

        public bool AdminE(Guid userID)
        {
            var user = this.userRepository.GetAll().First(x => x.UserID == userID);

            return user != null ? (bool)user.IsAdmin : false;
        }

        private Guid GetCategoryID(string categoryName)
        {
            return this.categoryRepository.GetAll().Where(x => x.Name == categoryName).Select(x => x.CategoryID).FirstOrDefault();
        }
    }
}
