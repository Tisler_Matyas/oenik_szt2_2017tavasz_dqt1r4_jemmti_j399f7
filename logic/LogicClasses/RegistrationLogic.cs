﻿// <copyright file="RegistrationLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace logic.LogicClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;
    using data.Interface;
    using data.Model;
    using Exceptions;
    using feleves_feladat;
    using global::Exceptions;
    using logic.Helpers;

    public class RegistrationLogic : IRegistration
    {
        private IUser userRepository;

        public RegistrationLogic(IUser pUserRepository)
        {
            this.userRepository = pUserRepository;
        }

        public void Registration(string fullName, string username, string email, string password)
        {
            if (this.userRepository.IsUsernameExists(username))
            {
                throw new UsernameExistsException("This username is already exists in the system");
            }

            if (this.userRepository.IsEmailExists(email))
            {
                throw new EmailExistsException("This email address is already exists in the system");
            }

            User newUser = new User();
            newUser.FullName = fullName;
            newUser.Username = username;
            newUser.Email = email;
            newUser.IsAdmin = false;
            newUser.Password = PasswordHelper.ComputeHash(password, new SHA256CryptoServiceProvider());
            newUser.Balance = 10000;

            this.userRepository.Save(newUser);
        }
    }
}
